import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export function regexp(source: Observable<string[]>): Observable<number> {
  return source.pipe(
    map(lines => [
      lines.filter((line) => line.indexOf(': ') !== -1),
      lines.filter((line) => line.indexOf(': ') === -1)
    ]),
    map(([rules, messages]) => {
      const rulesMap = rules
        .map(rule => rule.split(': '))
        .reduce((acc, [rule, value]) =>
          acc.set(rule, value),
          new Map<string, string>()
        );

      const regexp = new RegExp('^' + buildRegexp(rulesMap, rulesMap.get('0')) + '$');
      return messages
        .filter(message => message.match(regexp))
        .length;
    }),
  );
}

export function regexpPart2(source: Observable<string[]>): Observable<number> {
  return source.pipe(
    map(lines => [
      lines
        .concat(['8: 42 | 42 8', '11: 42 31 | 42 11 31'])
        .filter((line) => line.indexOf(': ') !== -1),
      lines.filter((line) => line.indexOf(': ') === -1)
    ]),
    map(([rules, messages]) => {
      let rulesMap = rules
        .map((rule) => rule.split(': '))
        .reduce((acc, [rule, value]) =>
          (value === '"a"' || value === '"b"')
            ? acc.set(rule, value[1])
            : acc.set(rule, value.split('|')
              .map(part => part.split(' ').filter(Boolean))),
          new Map<string, string[][] | string[] | string>()
        );

      return messages
        .map((message) => (rulesMap.get('0') as string[][]).some(rule => isValid(rulesMap, message, rule)))
        .filter(Boolean).length;
    })
  );
}

const buildRegexp = (rulesMap: Map<string, string>, rule: string): string => {
  let result = '';
  if (rule.indexOf('"') !== -1) {
    result += rule[1];
  
  } else if (rule.indexOf('|') !== -1) {
    let leftParts = rule.split(' | ')[0].split(' ');
    let rightParts = rule.split(' | ')[1].split(' ');
    
    result += '(';
    for (let i = 0; i < leftParts.length; i++) {
      result += buildRegexp(rulesMap, rulesMap.get(leftParts[i]));
    }
    result += '|';
    for (let i = 0; i < rightParts.length; i++) {
      result += buildRegexp(rulesMap, rulesMap.get(rightParts[i]));
    }
    result += ')';
    
  } else {
    for (let subRule of rule.split(' ')) {
      result += buildRegexp(rulesMap, rulesMap.get(subRule));
    }
  }
  return result;
}

const isValid = (rulesMap: Map<string, string[][] | string[] | string>, message: string, [rule, ...rest]: string[]): boolean => {
  if (!rule) {
    return message === '';
  }
  
  const ruleToEvaluate = rulesMap.get(rule);
  return (Array.isArray(ruleToEvaluate))
    ? (ruleToEvaluate as string[][]).some(rule => isValid(rulesMap, message, rule.concat(rest)))
    : message[0] === ruleToEvaluate && isValid(rulesMap, message.substr(1), rest);
}
