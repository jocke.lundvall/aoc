
export interface Food {
    ingredients: string[];
    alergens: string[];
}

