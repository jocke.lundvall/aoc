export interface Passport {
    byr?: string;
    iyr?: string;
    eyr?: string;
    hgt?: string;
    hcl?: string;
    ecl?: string;
    pid?: string;
    cid?: string;
}

export function isValid(passport: Passport): boolean {
    return passport != null
        && passport.byr != null
        && passport.iyr != null
        && passport.eyr != null
        && passport.hgt != null
        && passport.hcl != null
        && passport.ecl != null
        && passport.pid != null;
}

export function isValidExtendedCheck(passport: Passport): boolean {
    return passport
        && validYear(passport.byr, 1920, 2002)
        && validYear(passport.iyr, 2010, 2020) 
        && validYear(passport.eyr, 2020, 2030)
        && validHeight(passport.hgt)
        && validHexString(passport.hcl)
        && validEyeColor(passport.ecl)
        && validPassportId(passport.pid);
}

function validYear(year: string, min: number, max: number): boolean {
    return year && year.length === 4 && Number.parseInt(year) >= min && Number.parseInt(year) <= max;
}

function validHeight(height: string): boolean {
    if (!height) {
        return false;
    }
    let value = getHeightValue(height);
    return height.endsWith('cm')
        ? (value >= 150 && value <= 193)
        : (value >= 59 && value <= 76);
}

function validHexString(hex: string): boolean {
    return hex 
        && hex.length === 7
        && hex.startsWith('#')
        && Number.parseInt(hex.substr(1, 6), 16) !== NaN;
}

function validEyeColor(color: string): boolean {
    return ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].indexOf(color) !== -1;
}

function validPassportId(id: string): boolean {
    return id && id.length === 9 && Number.parseInt(id) !== NaN;
}

function getHeightValue(height: string): number {
    return Number.parseInt(height.replace('cm', '').replace('in', ''));
}