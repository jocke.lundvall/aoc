import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Bag, getBagsWithChild } from '../models/bag';

export function getPossibleParentBags(source: Observable<string[]>, child: string): Observable<Set<string>> {
  return source.pipe(
    map(bagInfos => {
      let allowedBags = new Set<string>();
      getParents(buildBagRules(bagInfos), allowedBags, child);
      return allowedBags;
    }),
  );
}

export function getNumberOfChildBags(source: Observable<string[]>, child: string): Observable<number> {
  return source.pipe(
    map(bagInfos => {
      let bagRules = buildBagRules(bagInfos);
      let num: number[] = [1];
      getChildren(bagRules, num, child);
      return num[0] - 1;
    }),
  );
}

function buildBagRules(bagInfos: string[]): Map<string, Bag> {
  let bagMap = new Map<string, Bag>();
  bagInfos.forEach(bagInfo => {
    let bagColor = bagInfo.substr(0, bagInfo.indexOf('bags') - 1);
    let bag = bagMap.get(bagColor) ?? {
      color: bagColor,
      children: [],
    };
    
    bagMap.set(bagColor, bag);

    let subBags = bagInfo.substr(bagInfo.indexOf('bags contain ') + 13).split(', ')
      .map((subBag) => subBag.replace('.', '').replace('bags', '').replace('bag', '').trim());

    subBags.filter((subBag => subBag != 'no other')).forEach((subBag) => {
      let amount = Number.parseInt(subBag.substr(0, subBag.indexOf(' ')));
      let subBagColor = subBag.substr(subBag.indexOf(' ') + 1);
      bag.children.push({color: subBagColor, amount });
    });
  });

  return bagMap;
}

function getParents(bagRules: Map<string, Bag>, allowed: Set<string>, query: string) {
  for (let parent of getBagsWithChild(bagRules, query)) {
    allowed.add(parent);
    getParents(bagRules, allowed, parent);
  }
}

function getChildren(bagRules: Map<string, Bag>, numberOfBags: number[], query: string) {
  let initialValue = numberOfBags[0];
  for (let child of bagRules.get(query).children) {
    let product = [initialValue * child.amount];
    getChildren(bagRules, product, child.color);
    numberOfBags[0] += product[0]; 
  }
}