export class Circle {

    circle = new Map<number, number>();
    current: number;

    constructor(inputs: number[]) {
        this.current = inputs[0];
        // Build ring of numbers
        for (let i = 0; i < inputs.length - 1; i++) {
            this.circle.set(inputs[i], inputs[i + 1]);
        }
        this.circle.set(inputs[inputs.length - 1], inputs[0]);
    }

    public doCupMove = (): void => {
        const nextAfterCurrent = this.circle.get(this.current);
        const secondNextAfterCurrent = this.circle.get(nextAfterCurrent);
        const thirdNextAfterCurrent = this.circle.get(secondNextAfterCurrent);
        const pickedUpsCups = [nextAfterCurrent, secondNextAfterCurrent, thirdNextAfterCurrent];
    
        const minPickLabel = Math.min(...pickedUpsCups);
        const minLabel = (minPickLabel > 1)
            ? 1
            : pickedUpsCups.includes(2)
                ? pickedUpsCups.includes(3)
                    ? 4
                    : 3
                : 2;
    
        let destinationCup = this.current - 1;
        while (destinationCup > minLabel && pickedUpsCups.includes(destinationCup)) {
          destinationCup--;
        }
    
        if (destinationCup < minLabel) {
          const maxPickLabel = Math.max(...pickedUpsCups);
          destinationCup =
            (maxPickLabel < this.circle.size)
              ? this.circle.size
              : pickedUpsCups.includes(this.circle.size - 1)
                ? pickedUpsCups.includes(this.circle.size - 2)
                    ? this.circle.size - 3
                    : this.circle.size - 2
                : this.circle.size - 1;
        }
    
        this.circle.set(this.current, this.circle.get(thirdNextAfterCurrent));
        this.current = this.circle.get(thirdNextAfterCurrent);
        this.circle.set(thirdNextAfterCurrent, this.circle.get(destinationCup));
        this.circle.set(destinationCup, nextAfterCurrent);
      }

}