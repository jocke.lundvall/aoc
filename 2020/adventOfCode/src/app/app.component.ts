import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { BehaviorSubject, forkJoin, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { getNumberOfChildBags, getPossibleParentBags } from './operators/bag-operators';
import { countMemoryValues } from './operators/bit-operators';
import { debugProgramAndRun, mapToInstructions, runProgram } from './operators/computer-operators';
import { reverseEngineer } from './operators/encryption-operators';
import { runCombatGame, runCupsGame, runCupsGamePart2, runMemoryGame, runRecursiveCombatGame } from './operators/game-operators';
import { getNumberOfDiffs, getNumberOfDistinctArrangements } from './operators/jolt-operators';
import { regexp, regexpPart2 } from './operators/match-operators';
import { calculate } from './operators/math-operators';
import { countTrees, gameOfLife, gameOfLife3D, gameOfLife4D } from './operators/matrix-operators';
import { calculateManhattanDistance, calculateTiles, findNextBus, findNextBusBonus } from './operators/navigation-operators';
import { countArrayEntries, countArrayOfSetsEntries, countSetEntries, findFirstXmasProtocolOutlier, findFirstXmasProtocolOutlierPartTwo, findXValuesWithSum, mapToFlightSeatId, mapToHighestNumber, mapToMySeatId, mapToNumbers, multiplyValues } from './operators/number-operators';
import { analyzeFood, filterValidPassports, filterValidPasswords, getCommonCustomsGroupAnswers, getCustomsGroupAnswers, getPassports, getPasswordInfos, mapToCsvEntries, mapToRows, mapToSections } from './operators/string-operators';
import { getScanningErrorRate, myTicket } from './operators/ticket-operators';
import { joinTiles } from './operators/tile-operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  private startTime: number;
  private calculation$: BehaviorSubject<number> = new BehaviorSubject(null);

  visibleDays = [...Array(25)].map((_, index) => index + 1);
  result$: Observable<number> = this.calculation$.asObservable();
  operationTime: number;

  constructor(private http: HttpClient) {}

  runAndTimeOperation(day: number, bonus = false) {
    this.startTimer$(
      this.getDayOperation$(day, bonus)
    ).pipe(
      (result) => this.stopTimer$(result)
    ).subscribe(
      (operationResult) => this.calculation$.next(operationResult),
      (err) => console.error(err),
      () => console.log('Operation completed')
    );
  }

  private getDayOperation$(day: number, bonus: boolean): Observable<number> {
    let input = this.getInput(day, false);
    switch(day) {
      case 1:
        return this.readNumberFile$(input).pipe(
          (numbers) => findXValuesWithSum(numbers, (bonus) ? 3 : 2, 2020),
          multiplyValues,
        );
        
      case 2:
        return this.readTextFileAsRows$(input).pipe(
          getPasswordInfos,
          (passwords) => filterValidPasswords(passwords, (bonus) ? 'index' : 'count'),
          countArrayEntries,
        );
        
      case 3:
        return this.readTextFileAsRows$(input).pipe(
          (result) => (bonus)
            ? forkJoin(
                [
                  countTrees(result, 1, 1),
                  countTrees(result, 3, 1),
                  countTrees(result, 5, 1),
                  countTrees(result, 7, 1),
                  countTrees(result, 1, 2),
                ]
              ).pipe(multiplyValues)
            : countTrees(result, 3, 1),
        );
      
      case 4:
        return this.readTextFile$(input).pipe(
          getPassports,
          (result) => filterValidPassports(result, bonus),
          countArrayEntries
        );

      case 5:
        return this.readTextFileAsRows$(input).pipe(
          mapToFlightSeatId,
          (bonus) ? mapToMySeatId : mapToHighestNumber,
        );

      case 6:
        return this.readTextFile$(input).pipe(
          mapToSections,
          (bonus) ? getCommonCustomsGroupAnswers : getCustomsGroupAnswers,
          countArrayOfSetsEntries,
        );

      case 7:
        return this.readTextFileAsRows$(input).pipe(
          (bonus)
            ? (result) => getNumberOfChildBags(result, 'shiny gold')
            : (result) => getPossibleParentBags(result, 'shiny gold').pipe(countSetEntries),
        );

      case 8: 
        return this.readTextFileAsRows$(input).pipe(
          mapToInstructions,
          (bonus) ? debugProgramAndRun : runProgram,
          map(result => result.accumulator),
        );

      case 9: 
        return this.readNumberFile$(input).pipe(
          (bonus)
            ? (result) => findFirstXmasProtocolOutlierPartTwo(result, 25)
            : (result) => findFirstXmasProtocolOutlier(result, 25),
        );

      case 10: 
        return this.readNumberFile$(input).pipe(
          (bonus) ? getNumberOfDistinctArrangements : getNumberOfDiffs,
        );
        
      case 11: 
        return this.readTextFileAsRows$(input).pipe(
          (result) => gameOfLife(result, bonus),
        );

      case 12: 
        return this.readTextFileAsRows$(input).pipe(
          (result) => calculateManhattanDistance(result, bonus),
        );
      
      case 13: 
        return this.readTextFileAsRows$(input).pipe(
          (bonus) ? findNextBusBonus : findNextBus,
        );

      case 14: 
        return this.readTextFileAsRows$(input).pipe(
          (result) => countMemoryValues(result, bonus),
        );

      case 15: 
        return this.readTextFile$(input).pipe(
          mapToCsvEntries,
          (result) => runMemoryGame(result, (bonus) ? 30000000 : 2020),
        );

      case 16: 
        return this.readTextFileAsRows$(input).pipe(
          (bonus) ? myTicket : getScanningErrorRate
        );

      case 17: 
        return this.readTextFileAsRows$(input).pipe(
          (bonus) ? gameOfLife4D : gameOfLife3D
        );

      case 18: 
        return this.readTextFileAsRows$(input).pipe(
          (result) => calculate(result, bonus),
        );

      case 19: 
        return this.readTextFileAsRows$(input).pipe(
          (bonus) ? regexpPart2 : regexp
        );

      case 20: 
        return this.readTextFile$(input).pipe(
          mapToSections,
          joinTiles
        );

      case 21: 
        return this.readTextFile$(input).pipe(
          mapToRows,
          analyzeFood
        );

      case 22: 
        return this.readTextFile$(input).pipe(
          mapToSections,
          bonus ? runRecursiveCombatGame: runCombatGame,
        );

      case 23: 
        return this.readTextFile$(input).pipe(
          (bonus) ? runCupsGamePart2 : runCupsGame,
        );

      case 24: 
        return this.readTextFileAsRows$(input).pipe(
         calculateTiles,
        );

      case 25: 
        return this.readTextFileAsRows$(input).pipe(
         reverseEngineer,
        );

      default:
        this.operationTime = 0;
        return of(NaN);
    }
  }

  private getInput(day: number, test = false): string {
    return `assets/${day}-input.` + (test ? 'test' : 'txt');
  }

  private readTextFile$(path: string): Observable<string> {
    return this.http.get(path, { responseType: 'text' });
  }

  private readTextFileAsRows$(path: string): Observable<string[]> {
    return this.readTextFile$(path).pipe(
      mapToRows,
    )
  }

  private readNumberFile$(path: string): Observable<number[]> {
    return this.readTextFile$(path).pipe(mapToNumbers);
  }

  private startTimer$<T>(source: Observable<T>): Observable<T> {
    this.startTime = Date.now();
    return source;
  }

  private stopTimer$<T>(source: Observable<T>): Observable<T> {
    return source.pipe(
      map(result => {
        this.operationTime = Date.now() - this.startTime;
        return result;
      })
    );
  }

}

