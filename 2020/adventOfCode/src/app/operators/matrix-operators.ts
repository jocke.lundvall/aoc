import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export function countTrees(source: Observable<string[]>, right: number, down: number) : Observable<number> {
  return source.pipe(
    map(forest => {
      var numTrees = 0;
      for (var row = 0; row < forest.length; row+=down) {
        let index = Math.max(0, (row/down * right) % forest[row].length);
        if (forest[row][index] === '#') {
          numTrees++;
        }
      }
      console.log('Number of trees found on path', numTrees);
      return numTrees;
    }),
  )
}

export function gameOfLife(source: Observable<string[]>, bonus: boolean): Observable<number> {
  return source.pipe(
    map(rows => {
      let current = createBoard(rows);
      let next = createBoard(rows);
      const aliveThreshold = (bonus) ? 5 : 4;
      const countFunction = (bonus) ? lineOfSightCount : neighboursCount;
      
      while (runSimulation(current, next, aliveThreshold, countFunction) !== 0) {
        copyAllValues(next, current);
      }
      return current.reduce(
            (acc, col) => acc.concat(col)
          )
          .reduce(
            (acc, value) => acc += (value === 1) ? 1 : 0,
            0
          );
    }),
  );
}

export function gameOfLife3D(source: Observable<string[]>): Observable<number> {
  return source.pipe(
    map(rows => {

      let current = new Set<string>();
      let next = new Set<string>();

      for (let i = 0; i < rows.length; i++) {
        for (let j = 0; j < rows[i].length; j++) {
          if (rows[i][j] === '#') {
            current.add(i + ',' + j + ',0');
            next.add(i + ',' + j + ',0');
          }
        }
      }

      let loops = 0;
      while(loops++ < 6) {
        console.log(loops);

        for (let active of current) {
          let sum = countActiveNeighbours(current, active);
          if (!(sum === 2 || sum === 3)) {
            next.delete(active);
          }
        }
        buildAllInactiveNeighbours(current, next);

        current.clear();
        next.forEach((value) => current.add((' ' + value).slice(1)));
      }
      return next.size;
    }),
  );
}

export function gameOfLife4D(source: Observable<string[]>): Observable<number> {
  return source.pipe(
    map(rows => {


      let current = new Set<string>();
      let next = new Set<string>();

      for (let i = 0; i < rows.length; i++) {
        for (let j = 0; j < rows[i].length; j++) {
          if (rows[i][j] === '#') {
            current.add(i + ',' + j + ',0,0');
            next.add(i + ',' + j + ',0,0');
          }
        }
      }

      let loops = 0;
      while(loops++ < 6) {
        console.log(loops);

        for (let active of current) {
          let sum = countActiveNeighbours4D(current, active);
          if (!(sum === 2 || sum === 3)) {
            next.delete(active);
          }
        }
        buildAllInactiveNeighbours4D(current, next);

        current.clear();
        next.forEach((value) => current.add((' ' + value).slice(1)));
      }
      console.log('alives', next.size);
      return next.size;
    }),
  );
}

const countActiveNeighbours = (current: Set<string>, toCheck: string): number => {
  let count = 0;
  let parts = toCheck.split(',').map((value) => parseInt(value));
  
  for(let i of [-1, 0, 1]) {
    for(let j of [-1, 0, 1]) {
      for(let k of [-1, 0, 1]) {
        if (!(i === 0 && j === 0 && k === 0)) {
          count += current.has((parts[0]+i) + ',' + (parts[1]+j) + ',' + (parts[2]+k)) ? 1 : 0;  
        }
      }
    }
  }

  return count;
}

const countActiveNeighbours4D = (current: Set<string>, toCheck: string): number => {
  let count = 0;
  let parts = toCheck.split(',').map((value) => parseInt(value));
  
  for(let i of [-1, 0, 1]) {
    for(let j of [-1, 0, 1]) {
      for(let k of [-1, 0, 1]) {
        for(let l of [-1, 0, 1]) {
          if (!(i === 0 && j === 0 && k === 0 && l === 0)) {
            count += current.has((parts[0]+i) + ',' + (parts[1]+j) + ',' + (parts[2]+k) + ',' + (parts[3]+l)) ? 1 : 0;
          }
        }
      }
    }
  }

  return count;
}

const buildAllInactiveNeighbours = (current: Set<string>, next: Set<string>): void => {
  for (let active of current) {
    let parts = active.split(',').map((value) => parseInt(value));

    for(let i of [-1, 0, 1]) {
      for(let j of [-1, 0, 1]) {
        for(let k of [-1, 0, 1]) {
          if (!(i === 0 && j === 0 && k === 0)) {
            const toCheck = (parts[0]+i) + ',' + (parts[1]+j) + ',' + (parts[2]+k);
            if (countActiveNeighbours(current, toCheck) === 3) {
              next.add(toCheck);
            }
          }
        }
      }
    }
  }
}

const buildAllInactiveNeighbours4D = (current: Set<string>, next: Set<string>): void => {
  for (let active of current) {
    let parts = active.split(',').map((value) => parseInt(value));

    for(let i of [-1, 0, 1]) {
      for(let j of [-1, 0, 1]) {
        for(let k of [-1, 0, 1]) {
          for(let l of [-1, 0, 1]) {
            if (!(i === 0 && j === 0 && k === 0 && l === 0)) {
              const toCheck = (parts[0]+i) + ',' + (parts[1]+j) + ',' + (parts[2]+k) + ',' + (parts[3]+l);
              if (countActiveNeighbours4D(current, toCheck) === 3) {
                next.add(toCheck);
              }
            }
          }
        }
      }
    }
  }
}

const copyAllValues = (from: number[][], to: number[][]) => {
  to.forEach((_, index) => to[index] = [...from[index]]);
}

function runSimulation(current: number[][], next: number[][],
    aliveThreshold: number,
    countFunction: (board: number[][], x: number, y: number) => number): number {
  let changes = 0;
  for (let y = 0; y < current.length; y++) {
    for (let x = 0; x < current[0].length; x++) {
      if (current[y][x] === -1) {
        // Do nothing
      }
      else if (!isOccupied(current, x, y)) {
        if (countFunction(current, x, y) === 0) {
          next[y][x] = 1;
          changes++;
        }
      } else {
        if (countFunction(current, x, y) >= aliveThreshold) {
          next[y][x] = 0;
          changes++;
        }
      }
    }
  }
  return changes;
}

const createBoard = (input: string[]): number[][] => {
  const board: number[][] = [];
  for (let i = 0; i < input.length; i++) {
    const row = [];
    for (let j = 0; j < input[i].length; j++) {
      const letter = input[i].substr(j, 1);
      if (letter === 'L') {
        row.push(0)
      } else if (letter === '#') {
        row.push(1);
      } else {
        row.push(-1);
      }
    }
    board.push(row);
  }
  return board;
}

const isOccupied = (board: number[][], x: number, y: number): boolean => {
  if (x < 0 || x >= board[0].length || y < 0 || y >= board.length) {
      return false;
  }
  return board[y][x] === 1;
}

const neighboursCount = (board: number[][], x: number, y: number): number => {
  let count = 0;
  for(let i of [-1, 0, 1]) {
      for(let j of [-1, 0, 1]) {
          if (!(i === 0 && j === 0)) {
              count += isOccupied(board, x + i, y + j) ? 1 : 0;
          }
      }
  }
  return count;
}

const lineOfSightCount = (board: number[][], x: number, y: number): number => {
  let count = findNextInSight(board, x, y, 'NW');
  count += findNextInSight(board, x, y, 'N');
  count += findNextInSight(board, x, y, 'NE');
  count += findNextInSight(board, x, y, 'W');
  count += findNextInSight(board, x, y, 'E');
  count += findNextInSight(board, x, y, 'SW');
  count += findNextInSight(board, x, y, 'S',);
  count += findNextInSight(board, x, y, 'SE');
  return count;
}

const findNextInSight = (board: number[][], x: number, y: number,
    direction: 'NW' | 'N' | 'NE' | 'W' | 'E' | 'SW' | 'S' | 'SE', step = 1): number => {
  let xStep = x + 
  ((direction.indexOf('W') !== -1)
    ? (step * -1)
    : (direction.indexOf('E') !== -1) ? step : 0);
  if (xStep < 0 || xStep >= board[0].length) {
    return 0;
  }

  let yStep = y + 
    ((direction.indexOf('N') !== -1)
      ? (step * -1)
      : (direction.indexOf('S') !== -1) ? step : 0);
  if (yStep < 0 || yStep >= board.length) {
    return 0;
  }

  return (board[yStep][xStep] === -1)
    ? findNextInSight(board, x, y, direction, step + 1)
    : board[yStep][xStep];
}
