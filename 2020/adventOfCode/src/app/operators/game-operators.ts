import { Observable } from 'rxjs';
import { map, mapTo } from 'rxjs/operators';
import { Circle } from '../models/circle';

export function runMemoryGame(source: Observable<string[]>, endTurn: number): Observable<number> {
  return source.pipe(
    map((startingNumbers) => {
      const numberToTurnArray = new Array<[secondPrevious: number, previous: number]>(endTurn + 1);
      for (let i = 0; i < startingNumbers.length; i++) {
        numberToTurnArray[parseInt(startingNumbers[i])] = [ null, i + 1 ];  
      }
      let turn = startingNumbers.length;
      let lastNumberSpoken = parseInt(startingNumbers[startingNumbers.length - 1]);
      let numberToSpeak: number;
      while (turn++ !== endTurn) {
        
        if (numberToTurnArray[lastNumberSpoken] == null) {
          numberToSpeak = Math.floor(Math.random() * 10);
              
        } else {
          const previousTurnsLastNumberSpoken = numberToTurnArray[lastNumberSpoken];
          numberToSpeak = (previousTurnsLastNumberSpoken && previousTurnsLastNumberSpoken[0]) 
            ? previousTurnsLastNumberSpoken[1] - previousTurnsLastNumberSpoken[0]
            : 0;
        }

        if (numberToTurnArray[numberToSpeak] != null) {
          const previousTurns = numberToTurnArray[numberToSpeak];
          previousTurns[0] = previousTurns[1];
          previousTurns[1] = turn;

        } else {
          numberToTurnArray[numberToSpeak] = [null, turn];
        }

        lastNumberSpoken = numberToSpeak;
      }
      
      return numberToSpeak;
    }),
  );
}

export const runCombatGame = (source: Observable<string[]>): Observable<number> => {
  return source.pipe(
    map(sections => sections
      .map(section => section.split('\n')
        .filter(part => part !== '' && !part.startsWith('Player'))
        .map(part => parseInt(part))
      )
    ),
    map(decks => {
      let playerOneDeck = decks[0];
      let playerTwoDeck = decks[1];

      while (playerOneDeck.length !== 0 && playerTwoDeck.length !== 0) {
        let playerOneCard = playerOneDeck[0];
        let playerTwoCard = playerTwoDeck[0];
        playerOneDeck = playerOneDeck.slice(1);
        playerTwoDeck = playerTwoDeck.slice(1);

        if (playerOneCard > playerTwoCard) {
          playerOneDeck.push(playerOneCard);
          playerOneDeck.push(playerTwoCard);

        } else if (playerTwoCard > playerOneCard) {
          playerTwoDeck.push(playerTwoCard);
          playerTwoDeck.push(playerOneCard);

        } else {
          console.log('Same card value');
          break;
        }
      }

      return ((playerOneDeck.length > playerTwoDeck.length) ? playerOneDeck : playerTwoDeck)
        .reduce((acc, value, index) => acc + value * (playerOneDeck.length + playerTwoDeck.length - index), 0);      
    }),
  );
}

export const runRecursiveCombatGame = (source: Observable<string[]>): Observable<number> => {
  return source.pipe(
    map(sections => sections
      .map(section => section.split('\n')
        .filter(part => part !== '' && !part.startsWith('Player'))
        .map(part => parseInt(part))
      )
    ),
    map(decks => {
      let playerOneDeck = decks[0];
      let playerTwoDeck = decks[1];
      const gameStates = new Set<string>();

      while (playerOneDeck.length !== 0 && playerTwoDeck.length !== 0) {
        [playerOneDeck, playerTwoDeck] = recursiveRound(gameStates, playerOneDeck, playerTwoDeck);
      }

      console.log(playerOneDeck, playerTwoDeck);

      return ((playerOneDeck.length > playerTwoDeck.length) ? playerOneDeck : playerTwoDeck)
        .reduce((acc, value, index) => acc + value * (playerOneDeck.length + playerTwoDeck.length - index), 0);       
    }),
  );
}

export const runCupsGame = (source: Observable<string>): Observable<number> => {
  return source.pipe(
    map(input => input.split('').map(num => parseInt(num))),
    map(cups => {
      let circle = new Circle(cups);
      for (let i = 0; i < 100; i++) {
        circle.doCupMove();
      }
      let currentCup = circle.circle.get(1);
      let result = "";
      while (currentCup != 1) {
        result += currentCup;
        currentCup = circle.circle.get(currentCup);
      }
      console.log(result);
    }),
    mapTo(NaN)
  );
}

export const runCupsGamePart2 = (source: Observable<string>): Observable<number> => {
  return source.pipe(
    map(input => input.split('').map(num => parseInt(num))),
    map(cups => {
      for (let i = 10; i <= 1000000; i++) {
        cups.push(i);
      }
      const circle = new Circle(cups);
      for (let i = 0; i < 10000000; i++) {
        circle.doCupMove();
      }
      const firstCup = circle.circle.get(1);
      const nextCup = circle.circle.get(firstCup);

      return firstCup * nextCup;
    }),
  );
}

const isGameStateOkv2 = (states: Set<string>, deck1: number[], deck2: number[]): boolean => {
  const state = deck1.join(' ') + ',' + deck2.join(' ');
  if (states.has(state)) {
    return false;
  }
  states.add(state);
  return true;
}

const recursiveRound = (states: Set<string>, deck1: number[], deck2: number[]): [number[], number[]] => {
  if (!isGameStateOkv2(states, deck1, deck2)) {
    return [deck1, []];
  }

  const playerOneCard = deck1.shift();
  const playerTwoCard = deck2.shift();
  let player1Wins = playerOneCard > playerTwoCard;

  if (playerOneCard <= deck1.length && playerTwoCard <= deck2.length) {
    let subDeck1 = deck1.slice(0, playerOneCard);
    let subDeck2 = deck2.slice(0, playerTwoCard);
    const subStates = new Set<string>();
    while (subDeck1.length !== 0 && subDeck2.length !== 0) {
      [subDeck1, subDeck2] = recursiveRound(subStates, subDeck1, subDeck2);
    }
    player1Wins = subDeck1.length > 0; 
  }

  if (player1Wins) {
    deck1.push(playerOneCard, playerTwoCard);
  } else {
    deck2.push(playerTwoCard, playerOneCard);
  }

  return [deck1, deck2];
}
