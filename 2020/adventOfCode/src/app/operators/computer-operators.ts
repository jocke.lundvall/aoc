import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ErrorCode, Instruction, OpCode, ProgramState } from '../models/computer';

export function mapToInstructions(source: Observable<string[]>): Observable<Instruction[]> {
  return source.pipe(
    map(result => {
      return result.map((row) => ({
        opCode: row.split(' ')[0] as OpCode,
        value: Number.parseInt(row.split(' ')[1])
      }));
    }),
  );
}

export function debugProgramAndRun(source: Observable<Instruction[]>): Observable<ProgramState> {
  return source.pipe(
    map(instructions => {
      var state: ProgramState = {
        programCounter: 0,
        errorCode: ErrorCode.undefined,
        accumulator: 0,
      }

      for (let i = 0; i < instructions.length; i++) {
        if (instructions[i].opCode === 'jmp') {
          state = swapOpCodeAndExecute(instructions, i, 'nop');
          
        } else if (instructions[i].opCode === 'nop' && instructions[i].value !== 0) {
          state = swapOpCodeAndExecute(instructions, i, 'jmp');
        }
        
        if (state.errorCode === ErrorCode.ok) {
          console.log('Found solution', i, state)
          return state;
        }
      }
      return null;
    })
  )
}

export function runProgram(source: Observable<Instruction[]>): Observable<ProgramState> {
  return source.pipe(
    map(execute),
  )
}

function swapOpCodeAndExecute(instructions: Instruction[], index: number, swapTo: OpCode): ProgramState {
  const oldOpCode = instructions[index].opCode;
  instructions[index].opCode = swapTo;
  const programState = execute(instructions);
  instructions[index].opCode = oldOpCode;
  return programState;
}

function execute(instructions: Instruction[]): ProgramState {
  const state: ProgramState = {
    programCounter: 0,
    accumulator: 0,
    errorCode: ErrorCode.ok,
  };
  const visitedOperations = new Set<number>();
  while(!isInExecutionError(state)
      && !isEndOfProgram(instructions, state)
      && !isInLiveLock(visitedOperations, state)) {
    executeInstruction(instructions[state.programCounter], state);
  }
  return state;
}

function executeInstruction(instruction: Instruction, state: ProgramState): void {
  switch(instruction.opCode) {
    case 'nop':
      state.programCounter++;
      break;
    case 'acc':
      state.accumulator += instruction.value;
      state.programCounter++;
      break;
    case 'jmp':
      state.programCounter += instruction.value;
      break;
    default:
      state.errorCode = ErrorCode.invalid_instruction;
      console.error(state);
      break;
  }
}

function isInLiveLock(visitedOperations: Set<number>, state: ProgramState): boolean {
  if (visitedOperations.has(state.programCounter)) {
    state.errorCode = ErrorCode.live_lock;
    return true;
  }
  visitedOperations.add(state.programCounter);
  return false;
}

function isEndOfProgram(instructions: Instruction[], state: ProgramState): boolean {
  return state.programCounter >= instructions.length;
}

function isInExecutionError(state: ProgramState): boolean {
  return state.errorCode < 0;
}