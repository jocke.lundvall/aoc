
export interface Tile {
    id: number;
    image: boolean[][];
}

export const tileToString = (tile: Tile): void => {
    console.log('Tile ' + tile.id + ':');
    for(let i = 0; i < tile.image.length; i++) {
        let row = '';
        for (let j = 0; j < tile.image[i].length; j++) {
            row += tile.image[i][j] ? '#' : '.';
        }
        console.log(row);
    }
    console.log('');
}

export const buildTile = (input: string): Tile => {
    const rows = input.split('\n').filter(row => row !== '');
    const id = parseInt(rows[0].slice(5).split(':')[0]);
    const image = new Array<boolean[]>(rows[1].length);

    for (let i = 1; i < rows.length; i++) {
        image[i-1] = new Array<boolean>(rows[i].length);
        for (let j = 0; j < rows[i].length; j++) {
            image[i-1][j] = rows[i][j] === '#';
        }
    }

    return { id, image };
}

export const buildSuperTile = (tiles: Tile[][]): Tile => {
    const id = 0;
    const image: boolean[][] = [];
    
    for (let i = 0; i < tiles.length * (tiles[0][0].image.length - 2); i++) {
        image.push(Array<boolean>(tiles.length * (tiles[0][0].image.length - 2)));
    }
    
    for (let i = 0; i < tiles.length; i++) {
        for (let j = 0; j < tiles[i].length; j++) {
            let tile = tiles[i][j];
            console.log(tile.id);
            for (let k = 1; k < tile.image.length - 1; k++) {
                for (let l = 1; l < tile.image[k].length - 1; l++) {
                    image[i * (tiles[0][0].image.length - 2) + (k - 1)][j * (tiles[0][0].image.length - 2) + (l - 1)] = tile.image[k][l];
                }
            }
        }
    }
    
    return {id, image};
}

export const rotate = (tile: Tile): Tile => {
    const n = tile.image.length;
    const x = Math.floor(n /  2);
    const y = n - 1;
    for (let i = 0; i < x; i++) {
        for (let j = i; j < y - i; j++) {
            let temp = tile.image[i][j];
            tile.image[i][j] = tile.image[y - j][i];
            tile.image[y - j][i] = tile.image[y - i][y - j];
            tile.image[y - i][y - j] = tile.image[j][y - i];
            tile.image[j][y - i] = temp;
        }
    }
    return tile;
}

export const flip = (tile: Tile): Tile => {
    const image = tile.image;
    for (let i = 0; i < image.length / 2; i++) {
        const temp = image[i];
        image[i] = image[image.length - 1 - i];
        image[image.length - 1 - i] = temp;
    }
    return tile;
}

export const fitsToLeft = (base: Tile, left: Tile): boolean => {
    for (let _ of [1, 2]) {
        for (let _ of [1, 2, 3, 4]) {
            let fits = true;
            for (let i = 0; i < left.image.length; i++) {
                if (left.image[i][left.image[i].length - 1] !== base.image[i][0]) {
                    fits = false;
                    break;
                }
            }
            if (fits) {
                return true;
            }

            left = rotate(left);
        }
        left = flip(left);
    }
    
    return false;
}

export const fitsToRight = (base: Tile, right: Tile): boolean => {
    for (let _ of [1, 2]) {
        for (let _ of [1, 2, 3, 4]) {
            let fits = true;
            for (let i = 0; i < right.image.length; i++) {
                if (base.image[i][base.image[i].length - 1] !== right.image[i][0]) {
                    fits = false;
                    break;
                }
            }
            if (fits) {
                return true;
            }

            right = rotate(right);
        }
        right = flip(right);
    }
    
    return false;
}

export const fitsAbove = (base: Tile, above: Tile): boolean => {
    for (let _ of [1, 2]) {
        for (let _ of [1, 2, 3, 4]) {
            let fits = true;
            for (let i = 0; i < above.image.length; i++) {
                if (base.image[0][i] !== above.image[above.image.length - 1][i]) {
                    fits = false;
                    break;
                }
            }
            if (fits) {
                return true;
            }

            above = rotate(above);
        }
        above = flip(above);
    }
    
    return false;
}

export const fitsBellow = (base: Tile, bellow: Tile): boolean => {
    for (let _ of [1, 2]) {
        for (let _ of [1, 2, 3, 4]) {
            let fits = true;
            for (let i = 0; i < bellow.image.length; i++) {
                if (base.image[base.image.length - 1][i] !== bellow.image[0][i]) {
                    fits = false;
                    break;
                }
            }
            if (fits) {
                return true;
            }

            bellow = rotate(bellow);
        }
        bellow = flip(bellow);
    }
    
    return false;
}

export const countTileMonsters = (tile: Tile): number => {
    let count = 0;
    for (let _ of [1, 2]) {
        for (let _ of [1, 2, 3, 4]) {
            for (let i = 0; i < tile.image.length - 20; i++) {
                for (let j = 0; j < tile.image[i].length - 3; j++) {
                    if ( 
                        tile.image[i + 18][j]
                        && tile.image[i][j+1]
                        && tile.image[i+5][j+1]
                        && tile.image[i+6][j+1]
                        && tile.image[i+11][j+1]
                        && tile.image[i+12][j+1]
                        && tile.image[i+17][j+1]
                        && tile.image[i+18][j+1]
                        && tile.image[i+19][j+1]
                        && tile.image[i+1][j+2]
                        && tile.image[i+4][j+2]
                        && tile.image[i+7][j+2]
                        && tile.image[i+10][j+2]
                        && tile.image[i+13][j+2]
                        && tile.image[i+16][j+2]
                    ) {
                        count++;
                        tile.image[i + 18][j] = false;
                        tile.image[i][j+1] = false;
                        tile.image[i+5][j+1] = false;
                        tile.image[i+6][j+1] = false;
                        tile.image[i+11][j+1] = false;
                        tile.image[i+12][j+1] = false;
                        tile.image[i+17][j+1] = false;
                        tile.image[i+18][j+1] = false;
                        tile.image[i+19][j+1] = false;
                        tile.image[i+1][j+2] = false;
                        tile.image[i+4][j+2] = false;
                        tile.image[i+7][j+2] = false;
                        tile.image[i+10][j+2] = false;
                        tile.image[i+13][j+2] = false;
                        tile.image[i+16][j+2] = false;
                    }
                }
            }
            if (count > 0) {
                return count;
            }
            tile = rotate(tile);
        }
        tile = flip(tile);
    }
    return count;
}

export const countTrueValues = (tile: Tile): number => {
    let count = 0;
    for (let i = 0; i < tile.image.length; i++) {
        for (let j = 0; j < tile.image[i].length; j++) {
            count += tile.image[i][j] ? 1 : 0;
        }
    }
    return count;
}

