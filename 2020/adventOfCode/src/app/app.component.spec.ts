import { HttpClientModule } from '@angular/common/http';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { timer } from 'rxjs';
import { take } from 'rxjs/operators';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ], imports: [HttpClientModule],
    }).compileComponents();
  }));

  it('should create the app and give correct answers', async () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();

    let nextResult = 0;
    let expectedResults =
      [null, 927684, 292093004, 410, 694, 151, 7540141059,
        239, 188, 970, 587, 6662, 3382, 161, 30899, 1801, 2060,
        41682220, 5388976, 2738, 74049191673856];
    app.result$.subscribe(value => {
      expect(value).toBe(expectedResults[nextResult++]);
    });

    for (let i = 1; i <= 10; i++) {
      app.runAndTimeOperation(i, false);
      await timer(100).pipe(take(1)).toPromise();
      app.runAndTimeOperation(i, true);
      await timer(100).pipe(take(1)).toPromise();
    }

  });

});
