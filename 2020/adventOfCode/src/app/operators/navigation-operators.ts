import { Observable } from 'rxjs';
import { map, mapTo } from 'rxjs/operators';

interface NavigationInfo {
  heading: number;
  currentX: number;
  currentY: number;
  waypointX: number;
  waypointY: number;
}

interface BusInfo {
  id: number;
  nextDeparture: number;
  loops?: number;
}

export function calculateManhattanDistance(source: Observable<string[]>, bonus: boolean): Observable<number> {
  return source.pipe(
    map(rows => {
      const navInfo: NavigationInfo = {
        heading: 90,
        currentX: 0,
        currentY: 0,
        waypointX: 10,
        waypointY: 1
      }
      for (const row of rows) {
        const instruction = row.substr(0, 1);
        const value = Number.parseInt(row.substring(1)); 
        (bonus)
          ? updateWaypointPosition(navInfo, instruction, value)
          : updateShipPosition(navInfo, instruction, value);
      }
     return Math.round(Math.abs(navInfo.currentX) + Math.abs(navInfo.currentY));
    }),
  );
}

export function findNextBus(source: Observable<string[]>): Observable<number> {
  return source.pipe(
    map((input) => {
      const target = Number.parseInt(input[0]);
      let earliest = Number.MAX_VALUE;
      let earliestBus: number;
      input[1].split(',').filter((value) => value !== 'x').forEach((value) => {
        const bus = Number.parseInt(value);
        const res = Math.ceil(target / bus);
        const x = res * bus;
        if (x < earliest) {
          earliest = x;
          earliestBus = bus;
        }
      });
      return (earliestBus * (earliest - target));
    }),
  );
}

export function findNextBusBonus(source: Observable<string[]>): Observable<number> {
  return source.pipe(
    map((input) => {
      const buses = input[1].split(',')
        .map((value) => (value === 'x') ? -1: Number.parseInt(value))
        .map((busId) => ({id: busId, nextDeparture: busId } as BusInfo));

      let allBusTimesIncrementsByOne = false;
      let loops = 1;
      let increment = 1;
      while (!allBusTimesIncrementsByOne) {
        allBusTimesIncrementsByOne = true;
        setBusDepartureTime(buses[0], buses[0].id * loops);

        for (let i = 1; i < buses.length; i++) {
          if (isOutOfService(buses[i])) {
            setBusDepartureTime(buses[i], buses[i-1].nextDeparture + 1);
            continue;
          }
          updateWithFirstPossibleTime(buses[i], buses[i-1], buses[0].nextDeparture);
          
          increment = (buses[i].loops == null) ? increment : loops - buses[i].loops;
          buses[i].loops = loops;

          if (!departsOneMinuteBefore(buses[i], buses[i - 1])) {  
            allBusTimesIncrementsByOne = false;
            break;
          }
        }
        loops += increment;
      }
      return buses[0].nextDeparture;
    }),
  );
}

type TileDirection = 'e' | 'se' | 'sw' | 'w' | 'nw' | 'ne';
export const calculateTiles = (source: Observable<string[]>): Observable<number> => {
  return source.pipe(
    map(rows => rows.map(row => {
      const directions: TileDirection[] = [];
      for (let i = 0; i < row.length; i++)  {
        if (row[i] === 'e' || row[i] === 'w') {
          directions.push(row.substr(i, 1) as TileDirection);
        } else {
          directions.push(row.substr(i, 2) as TileDirection);
          i++;
        }
      }
      return directions;
    })),
    map(directions => {
      let blackTiles = new Set<string>();
      for (let direction of directions) {
        const gridIndex: [number, number, number] = [0, 0, 0];
        for (let part of direction) {
          switch(part) {
            case 'e':
              gridIndex[0] += 1;
              gridIndex[2] -= 1;
              break;
            case 'se':
              gridIndex[1] += 1;
              gridIndex[2] -= 1;
              break;
            case 'sw':
              gridIndex[0] -= 1;
              gridIndex[1] += 1;
              break;
            case 'w':
              gridIndex[0] -= 1;
              gridIndex[2] += 1;
              break;
            case 'nw':
              gridIndex[1] -= 1;
              gridIndex[2] += 1;
              break;
            case 'ne':
              gridIndex[0] += 1;
              gridIndex[1] -= 1;
              break;
          }
        }
        const key = gridIndex[0] + ',' + gridIndex[1] + ',' + gridIndex[2];
        if (!blackTiles.has(key)) {
          blackTiles.add(key);
        } else {
          blackTiles.delete(key);
        }
      }
      console.log(blackTiles.size);


      // Part 2
      for (let i = 0; i < 100; i++) {
        let nextDay = new Set<string>();
        for (let blackTile of blackTiles.values()) {
          let numBlackNeighbours = countBlackNeighbours(blackTiles, blackTile);
          if (numBlackNeighbours === 1 || numBlackNeighbours === 2) {
            nextDay.add(blackTile);
          }
        }

        const whiteTiles = new Map<string, number>();
        countBlackNeghboursForWhiteTiles(blackTiles, whiteTiles);
        for (let entry of whiteTiles.entries()) {
          if (entry[1] === 2) {
            nextDay.add(entry[0]);
          }
        }

        console.log('next day', nextDay.size);
        blackTiles.clear();
        nextDay.forEach(tile => blackTiles.add(tile));
      }

    }),
    mapTo(NaN),
  );
}

const countBlackNeighbours = (blackTiles: Set<string>, tile: string): number => {
  let tileIndex: [number, number, number] = [0, 0, 0];
  tileIndex[0] = parseInt(tile.split(',')[0]);
  tileIndex[1] = parseInt(tile.split(',')[1]);
  tileIndex[2] = parseInt(tile.split(',')[2]);

  let numBlackNeighbours = 0;
  for(let i of [-1, 0, 1]) {
    for(let j of [-1, 0, 1]) {
      for(let k of [-1, 0, 1]) {
        if ((i === 0 && j === -1 && k === 1)
        || (i === 1 && j === -1 && k === 0)
        || (i === 1 && j === 0 && k === -1)
        || (i === 0 && j === 1 && k === -1)
        || (i === -1 && j === 1 && k === 0)
        || (i === -1 && j === 0 && k === 1)) {
          let key = (tileIndex[0] + i) + ',' + (tileIndex[1] + j) + ',' + (tileIndex[2] + k);
          if (blackTiles.has(key)) {
            numBlackNeighbours++;
          }
        }
      }
    }
  }
  return numBlackNeighbours;
}

const countBlackNeghboursForWhiteTiles = (blackTiles: Set<string>, whiteTiles: Map<string, number>): void => {
  for (let blackTile of blackTiles.values()) {
    let tileIndex: [number, number, number] = [0, 0, 0];
    tileIndex[0] = parseInt(blackTile.split(',')[0]);
    tileIndex[1] = parseInt(blackTile.split(',')[1]);
    tileIndex[2] = parseInt(blackTile.split(',')[2]);
    for(let i of [-1, 0, 1]) {
      for(let j of [-1, 0, 1]) {
        for(let k of [-1, 0, 1]) {
          if ((i === 0 && j === -1 && k === 1)
              || (i === 1 && j === -1 && k === 0)
              || (i === 1 && j === 0 && k === -1)
              || (i === 0 && j === 1 && k === -1)
              || (i === -1 && j === 1 && k === 0)
              || (i === -1 && j === 0 && k === 1)) {
            let key = (tileIndex[0] + i) + ',' + (tileIndex[1] + j) + ',' + (tileIndex[2] + k);
            if (!blackTiles.has(key)) {
              if (!whiteTiles.has(key)) {
                whiteTiles.set(key, 1);
              } else {
                whiteTiles.set(key, whiteTiles.get(key) + 1);
              }
            }
          }
        }
      }
    }
  }
}

const isOutOfService = (bus: BusInfo): boolean => bus.id === -1;
const setBusDepartureTime = (bus: BusInfo, time: number) => bus.nextDeparture = time;
const departsOneMinuteBefore = (bus: BusInfo, previousBus: BusInfo) => bus.nextDeparture === previousBus.nextDeparture + 1;
const updateWithFirstPossibleTime = (bus: BusInfo, previousBus: BusInfo, earliest: number): void => {
  let time = 0;
  let minLoops = Math.floor(earliest / bus.id);
  let additionalLoops = 1;
  while ((time - previousBus.nextDeparture) < 1) {
    time = bus.id * (minLoops + additionalLoops++);
  }
  setBusDepartureTime(bus, time);
}


const updateShipPosition = (navInfo: NavigationInfo, instruction: string, value: number) => {
  switch(instruction) {
    case 'N':
      navInfo.currentY += value;
      break;
    case 'S':
      navInfo.currentY -= value;
      break;
    case 'W':
      navInfo.currentX -= value;
      break;
    case 'E':
      navInfo.currentX += value;
      break;
    case 'F':
      const radians = degToRad(navInfo.heading);
      navInfo.currentX += Math.sin(radians) * value;
      navInfo.currentY += Math.cos(radians) * value;
      break;
    case 'R':
      navInfo.heading = (navInfo.heading + value) % 360;
      break;
    case 'L':
      navInfo.heading = (navInfo.heading - value) % 360;
      break;
    default:
      console.log('unknonw', instruction, value);
  }
  return navInfo;
}

const updateWaypointPosition = (navInfo: NavigationInfo, instruction: string, value: number) => {
  switch(instruction) {
    case 'N':
      navInfo.waypointY += value;
      break;
    case 'S':
      navInfo.waypointY -= value;
      break;
    case 'W':
      navInfo.waypointX -= value;
      break;
    case 'E':
      navInfo.waypointX += value;
      break;
    case 'F':
      navInfo.currentX += navInfo.waypointX * value;
      navInfo.currentY += navInfo.waypointY * value;
      break;
    case 'R':
      const newPosition = rotateAroundOrigo(0, 0, navInfo.waypointX, navInfo.waypointY, value);
      navInfo.waypointX = newPosition[0];
      navInfo.waypointY = newPosition[1];
      break;
    case 'L':
      const newPositionL = rotateAroundOrigo(0, 0, navInfo.waypointX, navInfo.waypointY, value * -1);
      navInfo.waypointX = newPositionL[0];
      navInfo.waypointY = newPositionL[1];
      break;
    default:
      console.log('unknonw', instruction, value);
  }
  return navInfo;
}

const degToRad = (degrees: number): number => {
  return degrees * (Math.PI / 180);
}

function rotateAroundOrigo(origoX: number, origoY: number,
    x: number, y: number, angle: number): [number, number] {
  const radians = degToRad(angle);
  const cos = Math.cos(radians);
  const sin = Math.sin(radians);
  return [
    (cos * (x - origoX)) + (sin * (y - origoY)) + origoX,
    (cos * (y - origoY)) - (sin * (x - origoX)) + origoY
  ];
}
