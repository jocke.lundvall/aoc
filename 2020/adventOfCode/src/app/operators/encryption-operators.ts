import { Observable } from 'rxjs';
import { map, mapTo } from 'rxjs/operators';

export const reverseEngineer = (source: Observable<string[]>): Observable<number> => {
  return source.pipe(
    map(rows => [parseInt(rows[0]), parseInt(rows[1])]),
    map(([cardPubKey, doorPubKey]) => {
      let loops = findLoops(cardPubKey, 20201227);
      let encryptionKey = transform(doorPubKey, loops, 20201227);
      console.log(loops);
      console.log(encryptionKey);
      // > 14897079
    }),
    mapTo(NaN)
  );
};

const findLoops = (key: number, base: number): number => {
  let i = 0;
  let value = 1;
  while (value !== key) {
    i++;
    value = ((value * 7) % base);
  }
  return i;
}

const transform = (key: number, loops: number, base: number): number => {
  let value = 1;
  for (let i = 0; i < loops; i++) {
    value = (value * key) % base;
  }
  return value;
}