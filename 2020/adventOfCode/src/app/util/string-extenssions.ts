export interface String {
    replaceAll(substr: string, newSubstr: string): string;
}