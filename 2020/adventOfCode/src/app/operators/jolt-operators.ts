import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { sort } from './number-operators';

export function getNumberOfDiffs(source: Observable<number[]>): Observable<number> {
  return source.pipe(
    sort,
    map(numbers => {
      let operationJoltage = 0;
      let diffs: number[] = [0, 0, 0, 1];
      for (let i = 0; i < numbers.length; i++) {
        let diff = numbers[i] - operationJoltage;
        if (diff === 1 || diff === 3) {
          diffs[diff]++;
        }
        operationJoltage = numbers[i];
      }
      return diffs[1] * diffs[3];
    }),
  );
}

export function getNumberOfDistinctArrangements(source: Observable<number[]>): Observable<number> {
  return source.pipe(
    sort,
    map(numbers => {
      numbers.push(numbers[numbers.length - 1] + 3)
      return countPaths(numbers);
    }),
  );
}

function countPaths(adapters: number[]) {
  const lookupMap = new Map<number, number>();
  lookupMap.set(adapters[adapters.length - 1], 1);
  for (let i = adapters.length - 2; i >= 0; i--) {
    lookupMap.set(adapters[i], getSumOfSubPaths(adapters[i], lookupMap));
  }
  
  return getSumOfSubPaths(0, lookupMap);
}

function getSumOfSubPaths(adapter: number, lookupMap: Map<number, number>): number {
  return (lookupMap.get(adapter + 1) ?? 0)
        + (lookupMap.get(adapter + 2) ?? 0)
        + (lookupMap.get(adapter + 3) ?? 0);
}
