
export interface Instruction {
    opCode: OpCode;
    value: number;
}

export interface ProgramState {
    programCounter: number,
    accumulator: number,
    errorCode: ErrorCode,
}

export enum ErrorCode {
    'ok' = 0,
    'live_lock' = -1,
    'invalid_instruction' = -2,
    'undefined' = Number.NEGATIVE_INFINITY,  
}
export type OpCode = 'nop' | 'acc' | 'jmp'; 