import { Observable } from 'rxjs';
import { map, mapTo } from 'rxjs/operators';
import { buildSuperTile, buildTile, countTileMonsters, countTrueValues, fitsAbove, fitsBellow, fitsToLeft, fitsToRight, flip, Tile, tileToString } from '../models/tile';

export function joinTiles(source: Observable<string[]>): Observable<number> {
  return source.pipe(
    map(sections => sections.map((section) => buildTile(section))),
    map(tiles => {
      tiles.forEach(tile => tileToString(tile));
      return tiles;
    }),
    map(tiles => {

      let tileMap = tiles.reduce((acc, tile) => 
        acc.set(tile.id, tile),
        new Map<number, Tile>()
      );

      let rows: Tile[][] = [];
      while (tileMap.size > 0) {
        let row: Tile[] = [];

        let currentTile: Tile;
        if (rows.length === 0) {
          currentTile = tileMap.values().next().value;
          tileMap.delete(currentTile.id);
          rows.push(row);

        } else {
          let found = false;
          let firstTileInLastRow = rows[rows.length - 1][0];
          for (let nextTile of tileMap.values()) {
            if (fitsBellow(firstTileInLastRow, nextTile)) {
              found = true;
              currentTile = nextTile;
              tileMap.delete(nextTile.id);
              rows.push(row);
              break;
            }
          }
          if (!found) {
            let firstTileInFirstRow = rows[0][0];
            for (let nextTile of tileMap.values()) {
              if (fitsAbove(firstTileInFirstRow, nextTile)) {
                found = true;
                currentTile = nextTile;
                tileMap.delete(nextTile.id);
                rows.unshift(row);
                break;
              }
            }
          }
          if (!found) {
            console.log('Error no mathing new row!', tileMap);
            return null;
          }
        }

        row.push(currentTile);
        let runAgain = false;
        do {
          runAgain = false;
          for (let nextTile of tileMap.values()) {
            if (fitsToLeft(currentTile, nextTile)) {
              row.unshift(nextTile);
              currentTile = nextTile;
              tileMap.delete(nextTile.id);
              runAgain = true; 
            }
          }
        } while(runAgain);

        currentTile = row[row.length - 1];
        do {
          runAgain = false;
          for (let nextTile of tileMap.values()) {
            if (fitsToRight(currentTile, nextTile)) {
              row.push(nextTile);
              currentTile = nextTile;
              tileMap.delete(nextTile.id);
              runAgain = true;
            }
          }
      } while (runAgain);

        console.log(row);
      }

      //console.log(rows[0][0].id * rows[0][11].id * rows[11][0].id * rows[11][11].id);

      let superTile = flip(buildSuperTile(rows));
      tileToString(superTile);
      console.log('True tiles', countTrueValues(superTile));
      console.log('Monsters', countTileMonsters(superTile));
      tileToString(superTile);
      console.log('True tiles', countTrueValues(superTile));
      return null;
    }),
    mapTo(NaN)
  );
}
