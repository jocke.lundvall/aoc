export interface PasswordInfo {
    password: string;
    rules: LetterRule[];
}

export interface LetterRule {
    letter: string;
    min: number;
    max: number;
}