import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export function calculate(source: Observable<string[]>, bonus: boolean): Observable<number> {
  return source.pipe(
    map(equations => equations.map(equation => equation.split(' '))),
    map(equations => (bonus) ? equations.map(equation => prepare(equation)) : equations),
    map(equations => equations.map(equation => solveEquation(equation))),
    map(equations => equations.reduce((acc, value) => acc + value, 0)),
  );
}

const prepare = (equationParts: string[]): string[] => {
  let numAdditions = equationParts.join(' ').split('+').length - 1;

  let nextIndex = -1;
  for (let i = 0; i < numAdditions; i++) {
    nextIndex = equationParts.indexOf('+', nextIndex + 1);
    surroundIndexWithParanthesis(equationParts, nextIndex);
  }
  return equationParts;
}

const surroundIndexWithParanthesis = (equationParts: string[], index: number): number => {
  if (equationParts[index - 1].endsWith(')')) {
    const start = findPartWithMatchingParentesisForward(equationParts, index - 1);
    equationParts[start] = '(' + equationParts[start];
  } else {
    equationParts[index - 1] = '(' + equationParts[index - 1];
  }
  
  if (equationParts[index + 1].startsWith('(')) {
    const end = findPartWithMatchingParentesis(equationParts, index + 1);
    equationParts[end] = equationParts[end] + ')';
  } else {
    equationParts[index + 1] = equationParts[index + 1] + ')';
  }

  return 0;
}

const solveEquation = (equationParts: string[]): number => {
  let value: number;
  let operation: 'MUL' | 'ADD';

  for (let i = 0; i < equationParts.length; i++) {
    if (equationParts[i].startsWith('(')) {
      let end = findPartWithMatchingParentesis(equationParts, i);
      equationParts[i] = equationParts[i].substr(1);
      equationParts[end] = equationParts[end].substr(0, equationParts[end].length - 1);
      
      let subEquationResult = solveEquation(equationParts.slice(i, end + 1));
      if (value == null) {
        value = subEquationResult;
      } else if (operation === 'ADD') {
        value += subEquationResult;
      } else if (operation === 'MUL') {
        value *= subEquationResult;
      }
      i = end;

    } else if (equationParts[i] === '+') {
      operation = 'ADD';
    } else if (equationParts[i] === '*') {
      operation = 'MUL';
    } else {
      if (value == null) {
        value = parseInt(equationParts[i]);
      } else if (operation === 'ADD') {
        value += parseInt(equationParts[i]);
      } else if (operation === 'MUL') {
        value *= parseInt(equationParts[i]);
      }
    }
  }
  return value;
}

const findPartWithMatchingParentesis = (equationParts: string[], indexWithStart: number): number => {
  let num = 0;
  for (let i = indexWithStart; i < equationParts.length; i++) {
    num += equationParts[i].split('(').length - 1;
    num -= equationParts[i].split(')').length - 1;
    if (num <= 0) {
      return i;
    }
  }
  return equationParts.length;
}

const findPartWithMatchingParentesisForward = (equationParts: string[], indexWithStart: number): number => {
  let num = 0;
  for (let i = indexWithStart; i >= 0; i--) {
    num += equationParts[i].split(')').length - 1;
    num -= equationParts[i].split('(').length - 1;
    if (num >= 0) {
      return i;
    }
  }
  return 1;
}