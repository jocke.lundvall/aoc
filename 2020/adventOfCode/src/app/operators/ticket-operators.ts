import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export function getScanningErrorRate(source: Observable<string[]>): Observable<number> {
  return source.pipe(
    map((lines) => {
      
      const allowed = new Set<number>();
      let errorsRate = 0;
      let ticketRowToAdd = false;
      for (let line of lines) {

        if (line.indexOf(' or ') !== -1) {
          const ranges = line.split(': ')[1].split(' or ');
          for (let range of ranges) {
            for (let i = parseInt(range.split('-')[0]); i <= parseInt(range.split('-')[1]); i++) {
              allowed.add(i);
            }
          }

        } else if (line === 'nearby tickets:') {
          ticketRowToAdd = true;

        } else if (ticketRowToAdd) {
          line.split(',').map((value) => parseInt(value)).forEach((value) => {
            if (!allowed.has(value)) {
              errorsRate += value;
            }
          });
        } 
      }

      return errorsRate;
    }),
  );
}

export function myTicket(source: Observable<string[]>): Observable<number> {
  return source.pipe(
    map((lines) => {
      
      const totalAllowed = new Set<number>();
      const alloweds: Array<number>[] = [];
      const values: Array<number>[] = [];
      const myTicket: Array<number> = [];
      let ticketRowToAdd = false;
      let myTicketRowToAdd = false;

      for (let line of lines) {

        if (line.indexOf(' or ') !== -1) {
          const ranges = line.split(': ')[1].split(' or ');
          const ruleSet = new Array<number>();
          alloweds.push(ruleSet);
          values.push(new Array<number>());

          for (let range of ranges) {
            for (let i = parseInt(range.split('-')[0]); i <= parseInt(range.split('-')[1]); i++) {
              totalAllowed.add(i);
              ruleSet.push(i);
            }
          }

        } else if (line === 'nearby tickets:') {
          ticketRowToAdd = true;
          myTicketRowToAdd = false;


        } else if (line === 'your ticket:') {
          myTicketRowToAdd = true;
          ticketRowToAdd = false;

        } else if (myTicketRowToAdd) {
          line.split(',').map((value) => parseInt(value)).forEach((value) => {
            myTicket.push(value);
          });
        
        } else if (ticketRowToAdd) {
          line.split(',').map((value) => parseInt(value)).forEach((value, index) => {
            if (totalAllowed.has(value)) {
              values[index].push(value);
            }
          });
        }
      }

      const doneValues = new Array<number>();
      const doneRules = new Array<number>();

      let result = 1;
      while(doneValues.length < values.length) {
        for (let i = 0; i < values.length; i++) {
          if (doneValues.indexOf(i) === -1) {
            let matchingCols = 0;
            let lastMatch = -1;
            for (let j = 0; j < alloweds.length; j++) {
              if (doneRules.indexOf(j) === -1) {
                const diff = difference(values[i], alloweds[j]).length;
                matchingCols +=  diff === 0 ? 1 : 0;
                if (diff === 0) {
                  lastMatch = j;
                }
              }
            }

            if (matchingCols === 1) {
              doneValues.push(i);
              doneRules.push(lastMatch);
              if (lastMatch < 6) {
                result *= myTicket[i];
              }
              break;
            }
          }
        }
      }

      return result;
    }),
  );
}

function difference(set1: Array<number>, set2: Array<number>): number[] {
  return [...set1].filter(x => set2.indexOf(x) === -1);
}