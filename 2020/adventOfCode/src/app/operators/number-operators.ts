import { forkJoin, Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { mapToRows } from './string-operators';

export function countArrayEntries(source: Observable<any[]>): Observable<number> {
  return source.pipe(
    map(array => array.length)
  )
}

export function countSetEntries(source: Observable<Set<any>>): Observable<number> {
  return source.pipe(
    map(set => set.size)
  )
}

export function countArrayOfSetsEntries(source: Observable<Set<any>[]>): Observable<number> {
  return source.pipe(
    map(array => {
      var total = 0;
      for (let item of array) {
        total += item.size;
      }
      return total;
    })
  );
}

export function multiplyValues(source: Observable<number[]>): Observable<number> {
  return source.pipe(
    map(numbers => numbers.reduce((acc, value) => acc * value))
  )
}

export function mapToNumbers(input: Observable<string>): Observable<number[]> {
  return input.pipe(
      mapToRows,
      map(rows => rows.map(row => Number.parseInt(row))
    )
  );
}

export function mapToHighestNumber(source: Observable<number[]>): Observable<number> {
  return source.pipe(
    map(values => Math.max(...values))
  );
}

export function sort(source: Observable<number[]>): Observable<number[]> {
  return source.pipe(
    map(values => values.sort((a, b) => a - b))
  );
}

export function mapToFlightSeatId(source: Observable<string[]>): Observable<number[]> {
  return source.pipe(
    map(bordingPasses => {
      return bordingPasses.map(pass => {
        let row = Number.parseInt(pass.substr(0, 7).split('F').join('0').split('B').join('1'), 2);
        let col = Number.parseInt(pass.substr(7, 3).split('L').join('0').split('R').join('1'), 2);
        return row * 8 + col;
      })
    }),
  );
}

export function mapToMySeatId(source: Observable<number[]>): Observable<number> {
  return source.pipe(
    sort,
    map((seatIds) => {
      for (var i = 1; i < seatIds.length; i++) {
        if(seatIds[i] - seatIds[i - 1] === 2) {
          return seatIds[i] - 1;
        }
      }
      return NaN;
    }),
  )
}

export function findFirstXmasProtocolOutlier(source: Observable<number[]>, order: number): Observable<number> {
  return source.pipe(
    map(numbers => {
      for (let numberIndex = order; numberIndex < numbers.length - order; numberIndex++) {
        if (!isSumOfTwoValuesInArray(numbers[numberIndex], numbers, numberIndex - order, order)) {
          return numbers[numberIndex];
        }
      }
      return NaN;
    }),
  );
}

export function findFirstXmasProtocolOutlierPartTwo(source: Observable<number[]>, order: number): Observable<number> {
  return source.pipe(
    switchMap(
      (result) => forkJoin(
        [
          findFirstXmasProtocolOutlier(of(result), order),
          of(result)
        ]
      )
    ),
    map(result => getContiguousNumbersThatSumTo(result[0], result[1]))
  );
}



export function findXValuesWithSum(source: Observable<number[]>, x: 2 | 3, sum: number): Observable<number[]> {
  return source.pipe(
    map(numbers => {
      if (x === 2) {
        for (var i = 0; i < numbers.length; i++) {
          for (var j = i; j < numbers.length; j++) {
            if (numbers[i] + numbers[j] === 2020) {
              return [numbers[i], numbers[j]];
            }
          }
        }
        
      } else if (x === 3) {
        for (var i = 0; i < numbers.length - 1; i++) {
          for (var j = i; j < numbers.length; j++) {
            for (var k = i + 1; k < numbers.length; k++) {
              if (numbers[i] + numbers[j] + numbers[k] === sum) {
                return [numbers[i], numbers[j], numbers[k]];
              }
            }
          }
        }
      }

      throw `Sum of $sum not found`;
    })
  );
}

function isSumOfTwoValuesInArray(sum: number, array: number[], offset: number, length: number): boolean {
  for (let i = offset; i < offset + length; i++) {
    for (let j = offset + 1; j < offset + length; j++) {
      if (i !== j && array[i] + array[j] === sum) {
        return true;
      }
    }
  }
  return false;
}

function getContiguousNumbersThatSumTo(sum: number, array: number[]): number {
  for (let i = 0; i < array.length; i++) {
    let total = array[i];

    let j = i;
    while(total < sum) {
      total += array[++j];
      if (total === sum) {
        let minMax = getMinAndMaxInArray(array, i, j-i);
        return minMax[0] + minMax[1];
      }
    }
  }
  return NaN;
}

function getMinAndMaxInArray(array: number[], offset: number, length: number): [min: number, max: number] {
  let min = Number.MAX_VALUE;
  let max = Number.MIN_VALUE;
  for (let i = offset; i < offset + length; i++) {
    min = array[i] < min ? array[i] : min;
    max = array[i] > max ? array[i] : max;
  }
  return [min, max];
}