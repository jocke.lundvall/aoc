import { Observable } from 'rxjs';
import { map, mapTo } from 'rxjs/operators';
import { Food } from '../models/food';
import { isValid, isValidExtendedCheck, Passport } from '../models/passport';
import { PasswordInfo } from '../models/password-info';

export function mapToRows(source: Observable<string>): Observable<string[]> {
  return source.pipe(
    map(text => text.split('\n').filter(text => text !== ''))
  )
}

export function mapToCsvEntries(source: Observable<string>): Observable<string[]> {
  return source.pipe(
    map(text => text.split(',').filter(text => text !== ''))
  )
}

export function mapToSections(source: Observable<string>): Observable<string[]> {
  return source.pipe(
    map(text => text.split('\n\n'))
  )
}

export function getPasswordInfos(source: Observable<string[]>): Observable<PasswordInfo[]> {
  return source.pipe(
    map(entries => 
      entries.map(entry => {
        let parts = entry.split(':');
        let min = parts[0].split('-')[0];
        let max = parts[0].split('-')[1].split(' ')[0];
        let letter = parts[0].split(' ')[1];
        return { password: parts[1].trim(), rules: [{min: Number.parseInt(min), max: Number.parseInt(max), letter}] }
      }),
    )
  );
}

export function getPassports(source: Observable<string>): Observable<Passport[]> {
  return source.pipe(
    mapToSections,
    map(sections => {
      return sections.map(section => {
        let parts = section.split(new RegExp([' ', ':', '\n'].join('|'), 'g')).filter((value) => value !== '');
        var index = 0;
        let passport: Passport = {};
        while (index < parts.length) {
          passport[parts[index].trim()] = parts[index + 1].trim();
          index +=2;
        }
        return passport;
      });
    })
  );
}

export function getCustomsGroupAnswers(source: Observable<string[]>): Observable<Set<string>[]> {
  return source.pipe(
    map(groupAnswers => {
      return groupAnswers.map(answers => {
        let uniqueAnswers = new Set<string>();
        for (var i = 0; i < answers.length; i++) {
          let letter = answers[i];
          if (letter !== '\n' && letter !== ' ') {
            uniqueAnswers.add(letter);
          }
        }
        return uniqueAnswers;
      });
    }),
  );
}

export function getCommonCustomsGroupAnswers(source: Observable<string[]>): Observable<Set<string>[]> {
  return source.pipe(
    map(groupAnswers => {
      return groupAnswers.map(answers => {
        let uniqueAnswers = new Set<string>();
        let personalAnswers = answers.split('\n')
        
        if (personalAnswers.length > 0) {
          for (var letter of personalAnswers[0]) {
            uniqueAnswers.add(letter);
            for (var otherPersonInGroup of personalAnswers) {
              if (otherPersonInGroup.length > 0 && otherPersonInGroup.indexOf(letter) === -1) {
                uniqueAnswers.delete(letter);
                break;
              }
            }
          }
        }

        return uniqueAnswers;
      });
    }),
  );
}

// > 3375

export function filterValidPassports(source: Observable<Passport[]>, extendedCheck: boolean): Observable<Passport[]> {
  return source.pipe(
    map(passports => passports.filter((extendedCheck) ? isValidExtendedCheck : isValid))
  )
}

export function filterValidPasswords(source: Observable<PasswordInfo[]>, type: 'count' | 'index'): Observable<PasswordInfo[]> {
  return source.pipe(
    map(passwords => passwords.filter(password => {
      for (var rule of password.rules) {
        
        if (type === 'count') {
          let occurences = password.password.split(rule.letter).length - 1;
          if (occurences > rule.max || occurences < rule.min) {
            return false;
          }

        } else if (type === 'index') {
          if ((password.password[rule.min - 1] !== rule.letter && password.password[rule.max - 1] !== rule.letter) || password.password[rule.min - 1] === password.password[rule.max - 1]) {
            return false;
          }
        }
      }
      return true;
    }))
  )
}

export const analyzeFood = (source: Observable<string[]>): Observable<number> => {
  return source.pipe(
    map(inputs => inputs.map(input => input.substr(0, input.length - 1).split(',').join('').split(' (contains '))),
    map(foods => 
      foods.map(food => ({
        ingredients: food[0].split(' '),
        alergens: food[1].split(' ')
      } as Food))
    ),
    map(foods => {
      console.log(foods);
      
      const allIngredients = foods.map(food => food.ingredients).reduce((acc, value) => acc.concat(value), []);

      const alergenCandidates = new Map<string, Set<string>>();
      for (let food of foods) {
        for (let alergen of food.alergens) {
          if (!alergenCandidates.has(alergen)) {
            alergenCandidates.set(alergen, new Set<string>(food.ingredients));
          }
          const possibleIngredients = alergenCandidates.get(alergen);

          let shouldBeOneOf = new Set<string>(food.ingredients);
          for (let ingredient of possibleIngredients) {
            if (!shouldBeOneOf.has(ingredient)) {
              possibleIngredients.delete(ingredient);
            }
          }
        }
      }

      let runAgain = true;
      while (runAgain) {
        runAgain = false;
        for (let alergenCandidate of alergenCandidates) {
          if (alergenCandidate[1].size === 1) {
            for (let otherAlergenCandidate of alergenCandidates) {
              if (otherAlergenCandidate[0] !== alergenCandidate[0]) {
                if (otherAlergenCandidate[1].has(alergenCandidate[1].values().next().value)) {
                  otherAlergenCandidate[1].delete(alergenCandidate[1].values().next().value);
                  runAgain = true;
                }
              }
            }
          }
        }
      }

      let ingredientToAlergen: Map<string, Set<string>> = allIngredients
        .reduce(
          (acc, value) => acc.set(value, new Set<string>()),
          new Map<string, Set<string>>()
        );
      for (let alergenCandidate of alergenCandidates) {
        for (let ingredient of alergenCandidate[1]) {
          ingredientToAlergen.get(ingredient).add(alergenCandidate[0]);
        }
      }
      

      let safeIngredients = new Set<string>(allIngredients);
      for (let ingredient of safeIngredients) {
        for (let alergenCandidate of alergenCandidates) {
          // if (alergenCandidate[1].size === 1 && alergenCandidate[1].values().next().value === ingredient) {
          //   safeIngredients.delete(ingredient);
          // }
          if (alergenCandidate[1].has(ingredient)) {
            safeIngredients.delete(ingredient);
          }
        }
      }

      let occurences = 0;
      for (let food of foods) {
        for (let ingredient of food.ingredients) {
          if (ingredientToAlergen.get(ingredient).size === 0) {
            occurences++;
          }
        }
      }
      
      let canonical: string[] = [];
      for (let alergen of new Map([...alergenCandidates.entries()].sort())) {
        canonical.push(alergen[1].values().next().value)
      }
      console.log(canonical.join(','));

      console.log('Alergen candidates', alergenCandidates);
      console.log('Ingredient to alergen', ingredientToAlergen);
      console.log('Safe ingredients', safeIngredients);
      console.log('Safe ingredients count', occurences);
      // console.log(ingredientLookup);

    }),

    // bthjz,gfvrr,jxcxh,mbkbn,mjbtz,ndkkq,pkkg,sgzr
    mapTo(NaN)
  );
}