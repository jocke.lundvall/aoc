
export interface Bag {
    color: string;
    children: { color: string, amount: number }[];
}

export function getBagsWithChild(bagRules: Map<string, Bag>, child: string): Set<string> {
    let allowedBags = new Set<string>();
    bagRules.forEach((rule) => {
        for (let bagChild of rule.children) {
            if (bagChild.color === child) {
                allowedBags.add(rule.color);
            }
        }
    });
    return allowedBags;
}