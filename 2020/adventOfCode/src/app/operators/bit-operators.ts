import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export function countMemoryValues(source: Observable<string[]>, bonus: boolean): Observable<number> {
  return source.pipe(
    map((lines) => {
      const programMemory = new Map<number, number>();
      let mask = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX';
      for (const line of lines) {
        if (line.startsWith('mask')) {
          mask = line.substr(7);
        } else if (line.startsWith('mem')) {
          const memoryAddress = parseInt(line.substring(4, line.indexOf(']')));
          const decimalValue = parseInt(line.substr(line.indexOf(' = ') + 3)); 
          if (bonus) {
            for (const address of getAllAddreses(decimalToBinaryString(memoryAddress), mask)) {
              programMemory.set(binaryToDecimal(address), decimalValue);
            }

          } else {
            programMemory.set(memoryAddress, binaryToDecimal(applyMask(decimalToBinaryString(decimalValue), mask)));
          }
        }
      }
      let sum = 0;
      for (const entry of programMemory.values()) {
        sum += entry;
      }
      return sum;
    }),
  );
}

const permutationsLookupMap = new Map<number, string[]>();
const createPermutation = (order: number): string[] => {
  let newPermutation: string[] = [];
  let maxDecimal = parseInt('1'.repeat(order), 2);
  for (let i = 0; i < maxDecimal + 1; i++) {
    newPermutation.push(i.toString(2).padStart(order, '0'));
  }
  permutationsLookupMap.set(order, newPermutation);
  return newPermutation;
}

const getAllAddreses = (address: string, mask: string): string[] => {
  for (let i = 0; i < mask.length; i++) {
    if (mask[i] === '1') {
      address = address.substr(0, i) + '1' + address.substring(i + 1);
    } else if (mask[i] === 'X') {
      address = address.substr(0, i) + 'X' + address.substring(i + 1);
    }
  }

  const amount = address.split('X').length;
  let permutation = permutationsLookupMap.has(amount)
    ? permutationsLookupMap.get(amount)
    : createPermutation(amount);

  const addresses = new Array<string>(permutation.length);
  for (let i = 0; i < permutation.length; i++) {
    let newAddress = address;
    for (let j = 0; j < permutation[i].length; j++) {
      newAddress = newAddress.replace('X', permutation[i][j]);
    }
    addresses[i] = newAddress;
  }
  return addresses;
}

const binaryToDecimal = (binary: string): number => parseInt(binary, 2);

const decimalToBinaryString = (decimal: number, paddedLength = 36): string => {
  let decimalString = decimal.toString(2);
  return decimalString.padStart(paddedLength, '0');
}

const applyMask = (value: string, mask: string): string => {
  for (let i = 0; i < mask.length; i++) {
    if (mask[i] !== 'X') {
      value = value.substr(0, i) + mask[i] + value.substring(i + 1);
    }
  }
  return value;
}